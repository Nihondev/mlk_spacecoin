﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dao.User;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.User
{
    public class ProductController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Product
        public ActionResult Index()
        {
            checkuser.chkrights("user");
            var data = ProductDao.Instance.GetDataList();
            int amount= GetDataDao.Instance.GetPointAmount(Varible.User.member_id);
            ViewBag.amount = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);

            string total = string.Empty;
            if (amount < 1000)
            {
                total = amount.ToString();
            }
            else
            {
                total = amount.ToString("0,0", CultureInfo.InvariantCulture);
            }
            Varible.User.amount = total;
            ViewBag.amount = Varible.User.amount;            
            return View(data);
        }
        //public ActionResult Product_order(int id)
        //{
        //    checkuser.chkrights("user");
        //    ViewBag.point = GetDataDao.Instance.GetPointAmount(Varible.User.member_id); //Varible.User.amount;
        //    ViewBag.amount = DropdownDao.Instance.GetMemberNo();
        //    var data = ProductDao.Instance.GetDataList().Find(smodel => smodel.id == id);           
        //    return View(data);
        //}

        public ActionResult Product_order(string Token)
        {
            checkuser.chkrights("user");
            try
            {            
                int id = Util.NVLInt(DataCryptography.Decrypt(Token));
                ViewBag.point = GetDataDao.Instance.GetPointAmount(Varible.User.member_id); //Varible.User.amount;
                ViewBag.amount = DropdownDao.Instance.GetProductAmt(id);
                var data = ProductDao.Instance.GetDataList().Find(smodel => smodel.id == id);
                return View(data);
            }
            catch(Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult SaveOrder(ItemListDto model)
        {
            try
            {
                int id = Util.NVLInt(DataCryptography.Decrypt(model.token));
                model.id = id;
                string result = ProductDao.Instance.SaveDataList(model);
                if (result != "OK")
                {
                    //return View(new { });
                    return Json(new returnsave { err = "1", errmsg = "Error" });
                }
                else
                {
                    TempData["Msg"] = "구매신청이 완료되었습니다.";
                    return View();
                    //return Json(new returnsave { err = "0", errmsg = "구매신청이 완료되었습니다." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }


        }

        [HttpPost]
        public ActionResult Product_order(ItemListDto model)
        {
            try
            {
                int id = Util.NVLInt(DataCryptography.Decrypt(model.token));
                model.id = id;

                //model.id = id;
                //model.price = Util.NVLString((amt * amount));// Util.NVLString((amount * amt));
                //model.point = (point - total);
                //model.percen = percen;
                //model.amount = amount;
                //model.chip = chip;
                //model.total = total;
                string result = ProductDao.Instance.SaveDataList(model);
                if (result != "OK")
                {
                    //return View(new { });
                    return Json(new returnsave { err = "1", errmsg = "Error" });
                }
                else
                {
                    ViewBag.amount = DropdownDao.Instance.GetMemberNo();
                    TempData["Msg"] = "구매신청이 완료되었습니다.";
                    return View();
                    //return Json(new returnsave { err = "0", errmsg = "구매신청이 완료되었습니다." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }


        }

        public ActionResult Product_onhand()
        {
           {
                return View();
            }
        }
        public ActionResult Product_cart()
        {
            {
                return View();
            }
        }

        //[HttpPost]
        //public ActionResult SaveOrder(int id, int point, int percen, int amount,int chip, int total,int amt)
        //{
        //    try
        //    {
        //        ItemListDto model = new ItemListDto();
        //        model.id = id;
        //        model.price = Util.NVLString((amt * amount));// Util.NVLString((amount * amt));
        //        model.point = (point - total);
        //        model.percen = percen;
        //        model.amount = amount;
        //        model.chip = chip;
        //        model.total = total;
        //        string result = ProductDao.Instance.SaveDataList(model, "buy");
        //        if (result != "OK")
        //        {
        //            return Json(new returnsave { err = "1", errmsg = "Error" });
        //        }
        //        else
        //        {
        //            return Json(new returnsave { err = "0", errmsg = "구매신청이 완료되었습니다." });
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(new returnsave { err = "1", errmsg = "Error" });
        //    }


        //}
    }
}