﻿using AHCBL.Component.Common;
using AHCBL.Dao.User;
using AHCBL.Dto.User;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace AHC_MLK.Controllers.Admin
{
    public class ChartMemberController : Controller
    {
        // GET: ChartMember
        public class level
        {
            public int id { get; set; }
            public string value { get; set; }
        }
        public ActionResult Index()
        {
            var id = Varible.User.member_id;

            var data = ChartMemberDao.Instance.GetChartData(id);

            var memberData = new List<ChartMemberDto>();
            data.ForEach(delegate (ChartMemberDto item)
            {
                if (item.id == id)
                    memberData.Add(item);
                if (item.master_id != 0)
                    memberData.Add(item);

            });

            List<level> levels = (
                    from lev in memberData
                    group lev by lev.adviser_id into level
                    select new level()
                    {
                        //id = level.Key,
                        value = level.Key.ToString()
                    }
                    ).ToList();


            Session["chart"] = memberData;
            List<SelectListItem> select = new List<SelectListItem>();
            for (int i = 0; i < levels.Count; i++)
            {
                select.Add(new SelectListItem()
                {
                    Text = "Level " + (i).ToString(),
                    Value = i.ToString(),
                });

                memberData.ForEach(delegate (ChartMemberDto member)
                {
                    if (member.adviser_id == Convert.ToInt32(levels[i].value))
                        member.level_id = i;
                });
            }
            Session["level"] = new SelectList(select, "Value", "Text");
            ViewBag.level = Session["level"];

            return View();
        }

        [WebMethod]
        public ActionResult GetChartData(string param)
        {
            try
            {
                System.Threading.Thread.Sleep(500);
                var id = Varible.User.member_id;
                ViewBag.level = Session["level"];
                var resultData = new List<ChartMemberDto>();
                var memberData = new List<ChartMemberDto>();

                var chartData = Session["chart"];
                memberData = ((IEnumerable)chartData).Cast<ChartMemberDto>().ToList();

                if (!string.IsNullOrEmpty(param))
                {
                    var data = ChartMemberDao.Instance.GetChartData(id);
                    var memberMaster = new List<ChartMemberDto>();

                    data.ForEach(delegate (ChartMemberDto item)
                    {
                        if (item.id == id)
                            memberMaster.Add(item);

                        if (item.master_id != 0)
                            memberMaster.Add(item);

                    });

                    if (Convert.ToInt32(param) == 0)
                    {
                        resultData = memberMaster;
                    }
                    else
                    {
                        memberData.RemoveAll(delegate (ChartMemberDto m)
                        {
                            return m.level_id < Convert.ToInt32(param);
                        });
                        resultData = memberData;
                    }
                }

                return Json(new { data = resultData });

            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}