﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class RequestBuyController : Controller
    {
        Permission checkuser = new Permission();
        // GET: RequestBuy
        public ActionResult Index(int? page, string drp, string keyword)
        {
            checkuser.chkrights("admin");
            var data = RequestBuyDao.Instance.GetDataList();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var count = data.Count;
            ViewBag.total = RequestListDao.Instance.GetTotal(Varible.User.member_id);
            ViewBag.PD1 = RequestListDao.Instance.GetPD1(Varible.User.member_id);
            ViewBag.PD2 = RequestListDao.Instance.GetPD2(Varible.User.member_id);
            ViewBag.Status = DropdownDao.Instance.GetStatusTran();
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();
            if (keyword != null)
            {
                if (drp == "username")
                {
                    TempData["data"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().Count;
                }
                if (drp == "date")
                {
                    TempData["data"] = data.Where(x => x.buy_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.buy_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.buy_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().Count;
                }
            }
            Session["Search"] = DropdownDao.Instance.GetDrpSearch();
            Session["Count"] = count;
            Session["Rows"] = rows;
            Session["data"] = TempData["data1"];

            ViewBag.Search = Session["Search"];
            ViewBag.Count = Session["Count"];
            ViewBag.Rows = Session["Rows"];

            return View(TempData["data"]);

        }
       

        [HttpPost]
        public ActionResult getdata(int orderId, int productId, int member)
        {
            try
            {

                string html = "";
                var Matching = RequestBuyDao.Instance.GetDataMatching(orderId,productId, member);
                if (Matching.Count > 0) {
                    for (int i = 0; i < Matching.Count; i++)
                    {
                        html += "<tr class='text-center'>" +
                            "<td> " + Matching[i].num + "</td>" +
                            "<td> " + Matching[i].username + "</td>" +
                            "<td> " + Matching[i].fullname + "</td>" +
                            "<td> " + Matching[i].product_name + " </td>" +
                            "<td> " + Matching[i].amount + "</td>" +
                            "<td> " + Matching[i].price + " </td>" +
                            "<td> " + Matching[i].buy_date + " </td>" +
                            "<td> <input type='button' value='Match' class='btn btn-secondary' onclick='matchdata(" + Matching[i].id + "," + Matching[i].amount + ")' /></td>" +
                        "</tr>";
                    }
                }
                else
                {
                    html += "<tr class='text-center'>" +
                                "<td colspan='8'>" +
                                    "No Data !!" +
                                "</td> " +
                            "</tr> ";
                }

                return Json(new returnsave { err = "0", errmsg = html });

            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }

        }
        public ActionResult matchdata(int id, int id_sale, int amt_sale)
        {
            try
            {
                //Session.Remove("member_id");dddd
                string result = RequestBuyDao.Instance.ChkData(id, id_sale, amt_sale);
                if (result == "0")
                {
                    return Json(new returnsave { err = "1", errmsg = "Error. !" });
                }
                else if (result == "1")
                {

                    return Json(new returnsave { err = "0", errmsg = "Successful. !" });
                }
                else if (result == "2")
                {
                    return Json(new returnsave { err = "0", errmsg = "Successful. !" });
                }
                else
                {
                    return Json(new returnsave { err = "0", errmsg = "Successful. !" });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error. !" });
            }
        }

    }
}