﻿using AHCBL.Component.Common;
using AHCBL.Dao.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class ProductOrderController : Controller
    {
        // GET: ProductOrder
        public ActionResult Index(int? page)
        {
            var data = ProductOrderDao.Instance.GetDataList();
            int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);
            string count = string.Empty;
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }
            ViewBag.Count = count;
            ViewBag.Rows = rows;
            return View(data.ToList().ToPagedList(page ?? 1, rows));

            
        }
    }
}