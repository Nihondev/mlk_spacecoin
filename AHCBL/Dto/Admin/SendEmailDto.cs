﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AHCBL.Dto.Admin
{
    public class SendEmailDto
    {
        public int id { get; set; }

        [MaxLength(50)]
        public string name { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string detail { get; set; }
        public string create_date { get; set; }
        public string active { get; set; }
    }
}
