﻿using AHCBL.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AHCBL.Dao
{
    public class FileUploadDao : BaseDao<FileUploadDao>
    {
        public FileUploadDto Upload(HttpPostedFileBase file)
        {
            try
            {
                FileUploadDto list = new FileUploadDto();
                if (file.ContentLength>0)
                {
                    string folder = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["path"]);
                    string _path = ConfigurationManager.AppSettings["path"];
                    string slash = ConfigurationManager.AppSettings["slash"];
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    string extension = Path.GetExtension(file.FileName);
                    string filename = "img_" + DateTime.Now.ToString("yyMMddHHmmss");
                    string path = Path.Combine(folder, filename + extension);
                    file.SaveAs(path);
                    list.path = ConfigurationManager.AppSettings["path"];
                    list.name = _path + slash + filename + extension;
                    list.type = extension;
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
