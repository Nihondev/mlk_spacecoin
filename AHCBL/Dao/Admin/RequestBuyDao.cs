﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AHCBL.Dto.Admin.RequestBuyDto;

namespace AHCBL.Dao.Admin
{
    public class RequestBuyDao : BaseDao<RequestBuyDao>
    {
        private MySqlConnection conn;
        private DataTable dt;

        public RequestBuyDto GetProductOrder(string id, string member)
        {
            try 
            { 

                RequestBuyDto data = new RequestBuyDto();
                dt = GetStoredProc("PD079_GET_MATCHING");
                foreach (DataRow dr in dt.Rows)
                {
                    data.tab1.Add(
                    new RequestBuy
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        product_id = Util.NVLInt(dr["product_id"]),
                        product_name = Util.NVLString(dr["product_name"]),
                        amount = Util.NVLString(dr["amount"]),
                        price = Util.NVLString(Util.NVLDecimal(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        buy_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        status = Util.NVLInt(dr["status"]),
                        create_by = Util.NVLInt(dr["create_by"]),
                    });
                }
                dt = GetStoredProc("PD080_GET_MATCHING_DATA", new string[] { "@p_id", "@member" }, new string[] { Util.NVLString(id), Util.NVLString(member) });
                foreach (DataRow dr in dt.Rows)
                {
                    data.tab2.Add(
                    new RequestBuy
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        product_name = Util.NVLString(dr["product_name"]),
                        amount = Util.NVLString(dr["amount"]),
                        create_by = Util.NVLInt(dr["create_by"]),
                        price = Util.NVLString(Util.NVLDecimal(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        buy_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        status = Util.NVLInt(dr["status"]),

                    });
                }
                return data;

            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }



        public List<RequestSellDto> GetDataList()
        {
            try
            {
                List<RequestSellDto> list = new List<RequestSellDto>();
                dt = GetStoredProc("PD079_GET_MATCHING");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new RequestSellDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        product_id = Util.NVLInt(dr["product_id"]),
                        create_by = Util.NVLInt(dr["create_by"]),
                        product_name = Util.NVLString(dr["product_name"]),
                        amount = Util.NVLString(dr["amount"]),
                        price = Util.NVLString(Util.NVLDecimal(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        buy_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        process = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMdd"))),
                        status = Util.NVLInt(dr["status"]),

                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }

        public List<RequestSellDto> GetDataMatching(int orderId, int productId, int member)
        {
            try
            {
                List<RequestSellDto> list = new List<RequestSellDto>();                
                dt = GetStoredProc("PD080_GET_MATCHING_DATA", new string[] { "@orderId", "@productId", "@member" }, new string[] { Util.NVLString(orderId), Util.NVLString(productId), Util.NVLString(member) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new RequestSellDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        username = Util.NVLString(dr["username"]),
                        fullname = Util.NVLString(dr["fullname"]),
                        product_name = Util.NVLString(dr["product_name"]),
                        amount = Util.NVLString(dr["amount"]),
                        create_by = Util.NVLInt(dr["create_by"]),
                        price = Util.NVLString(Util.NVLDecimal(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        buy_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        status = Util.NVLInt(dr["status"]),

                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public string ChkData(int id, int id_sale, int amt_sale)
        {
            string result = string.Empty;
            try
            {

                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD081_SAVE_MATCHING", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(id));
                AddSQLParam(param, "@id_sale", Util.NVLString(id_sale));
                AddSQLParam(param, "@amt_sale", Util.NVLString(amt_sale));

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();



            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                result = "0";
                throw ex;

            }
            return result;
        }

    }
}