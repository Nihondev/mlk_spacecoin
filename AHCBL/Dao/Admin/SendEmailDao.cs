﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class SendEmailDao : BaseDao<SendEmailDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<SendEmailDto> GetDataList()
        {
            try
            {
                List<SendEmailDto> list = new List<SendEmailDto>();
                dt = GetStoredProc("PD005_GET_EMAIL");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                        new SendEmailDto
                        {
                            id = Util.NVLInt(dr["id"]),
                            name = Util.NVLString(dr["name"]),
                            create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                            detail = Util.NVLString(dr["detail"])
                        });
                }
          
                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }


        public string SaveDataList(SendEmailDto model, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD002_SAVE_EMAIL", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@detail", Util.NVLString(model.detail));
                AddSQLParam(param, "@member_id", Util.NVLInt(1));
                AddSQLParam(param, "@active", Util.NVLInt(model.active));
                AddSQLParam(param, "@status", action);

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        public string Send(SendEmailDto model)
        {
            string result = "OK";
            try
            {

                SmtpClient SmtpServer = new SmtpClient("smtp.live.com");
                var mail = new MailMessage();
                mail.From = new MailAddress("suilon_02@hotmail.com");
                mail.To.Add("s6102042846145@email.kmutnb.ac.th");
                mail.Subject = model.name;
                mail.IsBodyHtml = true;
                string htmlBody;
                htmlBody = model.detail;
                mail.Body = htmlBody;
                SmtpServer.Port = 587;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential("suilon_02@hotmail.com", "kkCIYKWG1863");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);


            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}
