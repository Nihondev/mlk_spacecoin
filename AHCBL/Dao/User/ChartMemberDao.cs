﻿using AHCBL.Component.Common;
using AHCBL.Dto.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.User
{
    public class ChartMemberDao : BaseDao<ChartMemberDao>
    {
        private DataTable dt;
        /*
        public class xxx
        {
            public int id { get; set; }
            public string username { get; set; }
            public int adviser_id { get; set; }
            public string email { get; set; }
            public string create_date { get; set; }
        }


        public class Node
        {
            public string Key { get; }
            public List<xxx> data { get; }
        }

        public List<xxx> GetChartData()
        {
            try
            {

                List<xxx> list = new List<xxx>();
                dt = GetStoredProc("PD011_GET_CHART_MEMBER", new string[] { "@member_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new xxx
                    {
                        id= Util.NVLInt(dr["member_id"]),
                        username = Util.NVLString(dr["username"]),
                        adviser_id=Util.NVLInt(dr["adviser_id"]),
                        email=Util.NVLString(dr["email"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss")))
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        */
        public List<ChartMemberDto> GetChartData(int id)
        {
            try
            {

                List<ChartMemberDto> list = new List<ChartMemberDto>();
                dt = GetStoredProc("PD073_GET_CHART_MEMBER", new string[] { "@member_id" }, new string[] { Util.NVLString(id) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new ChartMemberDto
                    {
                        id = Util.NVLInt(dr["member_id"]),
                        username = Util.NVLString(dr["username"]),
                        adviser_id = Util.NVLInt(dr["adviser_id"]),
                        email = Util.NVLString(dr["email"]),
                        mobile = Util.NVLString(dr["mobile"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        master_id = Util.NVLInt(dr["master_id"])
                        //Util.NVLInt(dr["member_id"]), Util.NVLString(dr["username"]), Util.NVLString(dr["adviser_id"]), Util.NVLString(dr["email"]), Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss")))
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
    }
}